import os
import json
import time
import logging
import configparser

import flask
from flask_cors import CORS

from web3 import Web3
from solc import compile_source

web3 = Web3(Web3.HTTPProvider('http://eth:8545'))
if not web3.eth.accounts:
    web3.personal.newAccount('123')
account = web3.eth.accounts[0]
web3.personal.unlockAccount(account=account, passphrase="123", duration=None)

with open('/app/airtest.sol') as f:
    source_code = f.read()
compile_sol = compile_source(source_code)

MyContract = web3.eth.contract(
    abi = compile_sol['<stdin>:airtest']['abi'],
    bytecode = compile_sol['<stdin>:airtest']['bin'],
    bytecode_runtime = compile_sol['<stdin>:airtest']['bin-runtime']
)

trans_hash = MyContract.deploy(
    transaction={'from': web3.eth.accounts[0]},
    args=('admin', 'Airbus A320', 'SN001', 'Airbus, France', '24.03.2003')
)

# contract_address = '0xe246A62113B76A591bB8bd71796dC132EB62B480'

# wait for mining
web3.miner.start(1)
time.sleep(4)
web3.miner.stop()
trans_receipt = web3.eth.getTransactionReceipt(trans_hash)
print(trans_receipt)

# get the contract address
contract_address = trans_receipt['contractAddress']
print(contract_address)
#
my_contract = MyContract(contract_address)
# print(my_contract)

# CONFIG VARS

CONFIG = configparser.ConfigParser()
CONFIG.read('app.conf')
SECRET = CONFIG['app']['secret']

# FLASK APP & MYSQL


class NonASCIIJSONEncoder(json.JSONEncoder):
    def __init__(self, **kwargs):
        kwargs['ensure_ascii'] = False
        super(NonASCIIJSONEncoder, self).__init__(**kwargs)

app = flask.Flask(__name__)
app.secret_key = SECRET
app.json_encoder = NonASCIIJSONEncoder
cors = CORS(app, resources={r"*": {"origins": "*"}})

# LOGGING

if not os.path.exists('./log/'):
    os.makedirs('./log/')
logger = logging.getLogger('api')
api_handler = logging.FileHandler('./log/api.log')
# requests_handler = logging.StreamHandler()
api_formatter = logging.Formatter('%(asctime)s - %(levelname)-10s - %(message)s')
api_handler.setFormatter(api_formatter)
logger.addHandler(api_handler)
logger.setLevel(logging.INFO)
