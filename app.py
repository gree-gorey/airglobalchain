import time

import flask

from app_init import app, my_contract, account, web3, contract_address
from models import Aircraft

DEBUG = False


@app.route('/', strict_slashes=False, methods=['GET'])
def index():
    if flask.request.args.get('id'):
        if flask.request.args.get('id') != 'SN001':
            return flask.render_template('index.html', error='No information available for this ID')
        res = my_contract.call().getInfo()
        aircraft = Aircraft(flask.request.args.get('id'), *res[:3])
        aircraft.set_check(*res[3:])
        return flask.render_template('index.html', id=flask.request.args.get('id'), aircraft=aircraft)
    else:
        return flask.render_template('index.html')


@app.route('/check', strict_slashes=False, methods=['GET'])
def check_get():
    return flask.render_template('check.html', account=web3.eth.accounts[0])


@app.route('/check', strict_slashes=False, methods=['POST'])
def check_post():
    if flask.request.form.get('id') != 'SN001':
        return flask.render_template('check.html', error='Wrong ID')

    try:
        unlocked = web3.personal.unlockAccount(account=flask.request.form.get('account'),
                                               passphrase=flask.request.form.get('passphrase'),
                                               duration=None)
        if not unlocked:
            raise ValueError
    except ValueError:
        return flask.render_template('check.html', error='Wrong credentials')

    try:
        my_contract.transact({'from': account, 'to': contract_address}).doCheck(
            flask.request.form.get('date'),
            flask.request.form.get('name'),
            int(flask.request.form.get('ttsn')),
            int(flask.request.form.get('ttso')),
            int(flask.request.form.get('tcsn')),
            int(flask.request.form.get('tcso'))
        )
    except ValueError:
        return flask.render_template('check.html', error='Wrong value')
    except TypeError:
        return flask.render_template('check.html', error='Wrong value')
    web3.miner.start(1)
    time.sleep(2)
    web3.miner.stop()
    return flask.render_template('check.html', error='None')


def main():
    if DEBUG:
        app.run(
            host='0.0.0.0',
            port=8080,
            debug=True
        )
    else:
        app.run(
            host='0.0.0.0',
            port=8080,
            debug=False
        )


if __name__ == '__main__':
    main()
