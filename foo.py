with open('./airtest.sol') as f:
    source_code = f.read()

from web3 import Web3  # The `web3.Web3` object is all you should need to import in most normal cases.
web3 = Web3(Web3.HTTPProvider('http://localhost:8545'))
web3.personal.unlockAccount(account=web3.eth.accounts[0], passphrase="123", duration=None)

from solc import compile_source
compile_sol = compile_source(source_code)

# If an `address` is not passed into this method it returns a contract factory class.
MyContract = web3.eth.contract(
    abi = compile_sol['<stdin>:airtest']['abi'],
    bytecode = compile_sol['<stdin>:airtest']['bin'],   # The keyword `code` has been deprecated.  You should use `bytecode` instead.
    bytecode_runtime = compile_sol['<stdin>:airtest']['bin-runtime'],  # the keyword `code_runtime` has been deprecated.  You should use `bytecode_runtime` instead.
)

# trans_hash = MyContract.deploy(transaction={'from':web3.eth.accounts[0],'value':120})
#
# input()
# # wait for mining
# trans_receipt = web3.eth.getTransactionReceipt(trans_hash)
#
# # get the contract address
# contract_address = trans_receipt['contractAddress']

# now we can instantiate the contract factory to get an instance of the contract.
my_contract = MyContract('0xE87432864B70e8c86E98589E614f248659Dda83c')

# my_contract.transact({'from': web3.eth.accounts[0]}).doCheck(2009)

# now you should be able to call the contract methods.
res = my_contract.call().getCheckData()

print(res)
