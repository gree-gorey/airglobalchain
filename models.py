class Aircraft:
    def __init__(self, aircraft_id, model, manufacturer, manufacture_date):
        self.id = aircraft_id
        self.model = model
        self.manufacturer = manufacturer
        self.manufacture_date = manufacture_date
        self.check = None

    def set_check(self, date, examiner, ttsn, ttso, tcsn, tcso):
        self.check = Check(date, examiner, ttsn, ttso, tcsn, tcso)


class Check:
    def __init__(self, date, examiner, ttsn, ttso, tcsn, tcso):
        self.date = date
        self.examiner = examiner
        self.ttsn = ttsn
        self.ttso = ttso
        self.tcsn = tcsn
        self.tcso = tcso

