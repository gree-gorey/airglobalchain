source_code = 'contract MyToken {     address issuer;     mapping (address => uint) balances;      event Issue(address account, uint amount);     event Transfer(address from, address to, uint amount);      function MyToken() {         issuer = msg.sender;     }      function issue(address account, uint amount) {         if (msg.sender != issuer) throw;         balances[account] += amount;     }      function transfer(address to, uint amount) {         if (balances[msg.sender] < amount) throw;          balances[msg.sender] -= amount;         balances[to] += amount;          Transfer(msg.sender, to, amount);     }      function getBalance(address account) constant returns (uint) {         return balances[account];     } }'

import web3
obj = web3.Web3(web3.HTTPProvider('http://localhost:8545'))
psn = web3.personal.Personal(obj)
psn.unlockAccount(obj.eth.accounts[0],"123",1000) #replace 123456 to your Ethereum accounts password

from solc import compile_source
compile_sol = compile_source(source_code)

my_contract = web3.contract.construct_contract_factory(
    web3=obj,
    abi = compile_sol['<stdin>:MyToken']['abi'],
    code = compile_sol['<stdin>:MyToken']['bin'],
    code_runtime = compile_sol['<stdin>:MyToken']['bin-runtime'],
    source = source_code
)

my_contract.bytecode = my_contract.code
trans_hash = my_contract.deploy(transaction={'from':obj.eth.accounts[0],'value':120})

txn_receipt = web3.eth.getTransactionReceipt(trans_hash)
contract_address = txn_receipt['contractAddress']
my_contract.call({'to':contract_address}).getBalance(web3.eth.accounts[0])

