FROM ubuntu:xenial

WORKDIR /app

RUN apt-get -y update
RUN apt-get -y install python3 python3-pip
RUN pip3 install --upgrade pip

RUN apt-get -y install software-properties-common python-software-properties
RUN yes | add-apt-repository ppa:ethereum/ethereum
RUN apt-get -y update
RUN apt-get -y install solc

ADD requirements.txt /app

RUN pip3 install -r requirements.txt

COPY . /app

CMD python3 app.py
