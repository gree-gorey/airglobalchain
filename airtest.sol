pragma solidity ^0.4.2;

contract airtest {
     
     struct User {
          address addr;
          string name;
     }
     
     struct Aircraft {
         string ID;
         string model;
         string manufacturer;
         string manufactureDate;
     }
     
     struct Check {
         string date;
         string examiner;
         int TTSN;
         int TTSO;
         int TCSN;
         int TCSO;
     }

     User owner;
     Aircraft aircraft;
     Check check;
     mapping (address => User) adminInfo;
     mapping (address => bool) isAdmin;

     function airtest (string _admin_name, string _aircraft_model, string _aircraft_id,
                       string _manufacturer, string _manufacture_date) {
          owner = User({
               addr : msg.sender,
               name : _admin_name
          });

          aircraft = Aircraft({
              ID : _aircraft_id,
              model : _aircraft_model,
              manufacturer : _manufacturer,
              manufactureDate : _manufacture_date
          });

          isAdmin[msg.sender] = true;
          adminInfo[msg.sender] = owner;
     }

     // FUNCTIONS
     function addAdmin (address _address, string _name) {
          if (owner.addr != msg.sender || isAdmin[_address]) throw;

          isAdmin[_address] = true;
          adminInfo[_address] = User({addr : _address, name : _name});
     }

     function removeAdmin (address _address) {
          if (owner.addr != msg.sender || !isAdmin[_address]) throw;

          isAdmin[_address] = false;
          delete adminInfo[_address];
     }

     function doCheck (string _date, string _examiner, int _total_time_since_new, int _total_time_since_overhaul, int _total_cycles_since_new,
                       int _total_cycles_since_overhaul) {
          if (owner.addr != msg.sender) throw;

          check = Check({
              date : _date,
              examiner : _examiner,
              TTSN : _total_time_since_new,
              TTSO : _total_time_since_overhaul,
              TCSN : _total_cycles_since_new,
              TCSO : _total_cycles_since_overhaul
          });
     }

     function getInfo() constant returns (string, string, string, string, string, int, int, int, int) {
         return (aircraft.model, aircraft.manufacturer, aircraft.manufactureDate,
                 check.date, check.examiner,
                 check.TTSN, check.TTSO, check.TCSN, check.TCSO);
     }

     function killContract () {
          if (owner.addr != msg.sender) throw;
          selfdestruct(owner.addr);
     }
}